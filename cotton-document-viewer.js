var SELECTED = "selected";
var LOADED = "LOADED";
var PAGE = "PAGE";
var TIME = "TIME";
var CLEAR = "CLEAR";
var ACCEPT = "ACCEPT";
var PAGE_ITEM_DIV = ".pageItemImage";
var PAGE_ITEM_IMAGE = "";
var THUMB_ITEMS = "THUMB_ITEMS";
var THUMB_ITEM_IMAGE = ".thumbItemImage";
var PX = "px";
var TEMPLATE_SEPARATOR = "::";

var DocumentViewerTemplates = {
	"THUMBS_VIEW": "<div class=\"thumbsView\"></div>",
	"THUMB_ITEM": "<div class=\"thumbItem\"><div class=\"thumbItemImage layout horizontal\"><paper-spinner active></paper-spinner><div class=\"pageNumber\">::PAGE::</div></div></div>",
	"PAGES_VIEW": "<div class=\"pagesView\"/>",
	"PAGE_ITEM": "<div class=\"pageItem\"><div class=\"pageItemImage layout horizontal\"><paper-spinner active></paper-spinner></div></div>",
	"LABEL": "<div class=\"pagesLabel\"><div class=\"dialog title\"></div></div>",
	"LABEL_EDITABLE": "<div class=\"pagesLabel\"><div class=\"dialog\"><input type=\"text\" class=\"title\"/><a class=\"behaviour accept\" style=\"display:none;\">::ACCEPT::</a><a class=\"behaviour clear\" alt=\"::CLEAR::\"></a></div></div>"
};

var DocumentViewerLang = {
	es: {
		DocumentInProgress: "<span>El documento se está procesando, tiempo estimado para terminar ::TIME:: segundos.</span>",
		Accept: "aceptar",
		Clear: "borrar"
	},
	en: {
		DocumentInProgress: "<span>Document generation is in progress, estimated time to finish is ::TIME:: seconds.</span>",
		Accept: "accept",
		Clear: "delete"
	}
};

var ThumbViewer = function (eThumb, item, urlResolver) {
	this.item = item;
	this.eControl = eThumb;
	this.selectedHandler = null;
	this.urlResolver = urlResolver;

	this.eThumbItemImage = this.eControl.find(THUMB_ITEM_IMAGE);
};
ThumbViewer.prototype = {

	getId: function () {
		return this.item.id;
	},
	isLoaded: function () {
		return this.eControl.hasClass(LOADED);
	},

	load: function () {
		if (!this.isLoaded()) {
			var img = DocumentViewer.appendElement(this.eThumbItemImage, '<img src="' + this.urlResolver.resolve(this.item.id) + '"/>');
			img.click(this.onClick.bind(this));
			this.eControl.addClass(LOADED);
		}
	},

	onClick: function () {
		if (this.selectedHandler != null)
			this.selectedHandler(this);
	},

	setSelected: function (selected) {
		if (selected) {
			this.eControl.addClass(SELECTED);
			if (this.outOfViewportHandler != null) this.outOfViewportHandler(this.eControl);
		}
		else
			this.eControl.removeClass(SELECTED);
	},

	setClickHandler: function (delegate) {
		this.selectedHandler = delegate;
	},

	setOutOfViewportHandler: function (delegate) {
		this.outOfViewportHandler = delegate;
	},

	dispose: function () {
		this.eControl = null;
	}
};

var PageViewer = function (eThumb, item, urlResolver) {
	this.item = item;
	this.label = null;
	this.labelEditable = false;
	this.eControl = eThumb;
	this.selectedHandler = null;
	this.fZoomFactor = 0.75;
	this.ePageItemImage = this.eControl.find(PAGE_ITEM_DIV);
	this.ePageItemImage.css({ width: item.width * this.fZoomFactor + PX, height: item.height * this.fZoomFactor + PX});
	this.urlResolver = urlResolver;
};

PageViewer.prototype = {

	getId: function () {
		return this.item.id;
	},
	isLoaded: function () {
		return this.eControl.hasClass(LOADED);
	},

	load: function () {
		if (!this.isLoaded()) {
			var img = DocumentViewer.appendElement(this.ePageItemImage, '<img src="' + this.urlResolver.resolve(this.item.id) + '"/>');
			img.css({ width: this.item.width * this.fZoomFactor + PX, height: this.item.height * this.fZoomFactor + PX});
			this.ePageImage = img;
			this.ePageImage.on("click", this.atPageClick, this);
			this.eControl.addClass(LOADED);
		}
	},

	setZoom: function (zoomFactor) {
		this.fZoomFactor = zoomFactor;
		this.ePageItemImage.css({ width: this.item.width * this.fZoomFactor + PX,
			height: this.item.height * this.fZoomFactor + PX});
		if (this.ePageImage) {
			this.ePageImage.css({ width: this.item.width * this.fZoomFactor + PX,
				height: this.item.height * this.fZoomFactor + PX});
		}
	},

	width: function () {
		return this.item.width;
	},

	select: function (container) {
		var scrollTo = this.eControl;
		var containerEl = $(container);
		containerEl.animate({scrollTop: containerEl.offset().top + scrollTo.offset().top});
		this.load();
	},

	setClickHandler: function (delegate) {
		this.selectedHandler = delegate;
	},

	dispose: function () {
		this.eControl = this.ePageItemImage = this.ePageImage = null;
	},

	getPageHeight: function () {
		return this.eControl.height();
	},

	getPageTop: function () {
		return this.eControl.offset().top;
	},

	getPageBottom: function () {
		return this.eControl.offset().top + this.eControl.height();
	},

	atPageClick: function () {
		if (this.onClick) this.onClick(this);
	}
};

var DocumentViewer = function (thumbsContainer, pagesContainer, buttonsContainer, sLang) {
	this.sLang = sLang ? sLang : "en";
	this.aThumbViewers = new Array();
	this.aPageViewers = new Array();
	this.thumbContainer = null;
	this.pageContainer = null;
	this.iSelectedPage = -1;
	this.bInitialized = false;

	this.thumbsContainer = thumbsContainer;
	this.pagesContainer = pagesContainer;
	this.buttonsContainer = buttonsContainer;

	this.fZoomFactor = 0.75;
	this.fZoomFactorIncr = 0.25;
};
DocumentViewer.appendElement = function(container, content) {
	var element = $(content);
	$(container).append(element);
	return element;
};
DocumentViewer.prototype = {
	dispose: function () {
		for (var i = 0; i < this.aThumbViewers.length; i++)
			this.aThumbViewers[i].dispose();
		for (var i = 0; i < this.aPageViewers.length; i++)
			this.aPageViewers[i].dispose();
	},

	setDocumentLabel: function (label, editable) {
		this.label = label;
		this.labelEditable = editable;
	},

	showDocumentLabel: function () {
		this.labelVisible = true;
	},

	hideDocumentLabel: function () {
		this.labelVisible = false;
	},

	setDocumentUrl: function(url) {
		this.documentUrl = url;
	},

	setDocumentPagePath: function (path) {
		this.pagePath = path;
	},

	setDocumentPageThumbPath: function (path) {
		this.pageThumbPath = path;
	},

	init: function () {
		if (this.currentPage != null)
			return;

		this.currentPage = document.createElement("div");
		var currentPageElement = $(this.currentPage);
		currentPageElement.addClass("page")
		currentPageElement.change(this.onCurrentPageChanged.bind(this));
		currentPageElement.keyup(this.onCurrentPageKeyPress.bind(this));

		$(window).on('scroll', function() {
		    var scrollTop = $(window).scrollTop();
		    var thumbsContainer = $(this.thumbsContainer);
		    var buttonsContainer = $(this.buttonsContainer);
		    if (scrollTop > 50) {
		    	thumbsContainer.addClass("minimized");
		    	buttonsContainer.addClass("minimized");
		    }
		    else {
		    	thumbsContainer.removeClass("minimized");
		    	buttonsContainer.removeClass("minimized");
		    }
		    this.onScrollPageView();
		}.bind(this));

		var zoomInButton = $('<paper-icon-button icon="zoom-in"></paper-icon-button>');
		zoomInButton.get(0).addEventListener("tap", this.onZoomIn.bind(this));
		$(this.buttonsContainer).append(zoomInButton);
		
		var zoomOutButton = $('<paper-icon-button icon="zoom-out"></paper-icon-button>');
		zoomOutButton.get(0).addEventListener("tap", this.onZoomOut.bind(this));
		$(this.buttonsContainer).append(zoomOutButton);
	},

	setViewPort: function (viewPort) {
		this.pageViewPort = viewPort;
	},

	onCurrentPageKeyPress: function (extTextField, event) {
		if (event.getKey() == Ext.EventObject.ENTER) {
			this.onCurrentPageChanged(extTextField, extTextField.getValue());
		}
	},

	onCurrentPageChanged: function (extTextField, newValue, oldValue) {
		var iValue = parseInt(newValue) - 1;
		if (iValue >= 0 && iValue < (this.aThumbViewers.length)) {
			this.changePage(iValue, true);
		}
	},

	updateLabel: function () {
		if (this.label != null) {
			var pagesContainer = $(this.pagesContainer);
			var eDialog = pagesContainer.select(".dialog").first();
			eDialog.width(this.aPageViewers[0].width() * this.fZoomFactor);
		}
	},

	updateAllPages: function () {
		var totalPages = this.aPageViewers.length;
		var iMaxWidth = -1;

		for (var i = 0; i < totalPages; i++) {
			this.aPageViewers[i].setZoom(this.fZoomFactor);
			var iCurrentWidth = this.aPageViewers[i].width();
			if (iCurrentWidth > iMaxWidth) iMaxWidth = iCurrentWidth;
		}

		//if ((totalPages > 0) && (this.fZoomFactor >= 1)) this.pageContainer.width((iMaxWidth * this.fZoomFactor) + this.pageContainer.get(0).style.paddingRight);

		this.aPageViewers[this.iSelectedPage].select(this.pageContainer);
		this.checkPageLoad();
	},

	onZoomIn: function () {
		if (this.bInitialized) {
			this.fZoomFactor += this.fZoomFactorIncr;
			this.updateLabel();
			this.updateAllPages();
		}
	},

	onZoomOut: function () {
		if (this.bInitialized) {
			if (this.fZoomFactor - this.fZoomFactorIncr >= (this.fZoomFactorIncr * 2))
				this.fZoomFactor -= this.fZoomFactorIncr;
			this.updateLabel();
			this.updateAllPages();
		}
	},

	onGoToPrevious: function () {
		if (this.bInitialized)
			this.changePage(this.iSelectedPage - 1, true);
	},

	onGoToNext: function () {
		if (this.bInitialized)
			this.changePage(this.iSelectedPage + 1, true);
	},

	load: function () {
		this.init();

		$.ajax({
			url: this.documentUrl,
			dataType: "json", 
			method: "GET"
		}).done(function (response, options) {
				this.onMetadataReceived(response);
		}.bind(this)).fail(function (response, options) {
			console.log("error loading document viewer: " + response.responseText);
		}.bind(this));
	},

	onMetadataReceived: function (metadata) {
		var pagesContainer = $(this.pagesContainer);

		pagesContainer.get(0).innerHTML = "";

		if (!metadata.hasPendingOperations) {
			var numberOfPages = metadata.numberOfPages;

			this.thumbContainer = DocumentViewer.appendElement(this.thumbsContainer, DocumentViewerTemplates.THUMBS_VIEW);
			this.pageContainer = $(this.pagesContainer);

			for (var i = 1; i <= numberOfPages; i++) {
				var ePageControl = DocumentViewer.appendElement(this.pageContainer, DocumentViewerTemplates.PAGE_ITEM);
				var oPageViewer = new PageViewer(ePageControl, metadata.pages[i], {
					documentUrl: this.documentUrl,
					pagePath: this.pagePath,

					resolve: function(page) {
						var result = this.documentUrl;
						result += this.pagePath.replace(":page", page);
						return result;
					}
				});

				oPageViewer.onClick = this.atPageClick.bind(this);
				this.aPageViewers.push(oPageViewer);

				var eThumbControl = DocumentViewer.appendElement(this.thumbContainer, DocumentViewerTemplates.THUMB_ITEM.replace(TEMPLATE_SEPARATOR + PAGE + TEMPLATE_SEPARATOR, i));
				var oThumbViewer = new ThumbViewer(eThumbControl, metadata.pages[i], {
					documentUrl: this.documentUrl,
					pageThumbPath: this.pageThumbPath,

					resolve: function(page) {
						var result = this.documentUrl;
						result += this.pageThumbPath.replace(":page", page);
						return result;
					}
				});
				oThumbViewer.setClickHandler(this.onThumbSelected.bind(this));
				oThumbViewer.setOutOfViewportHandler(this.onThumbOutOfViewport.bind(this));
				oThumbViewer.load();
				this.aThumbViewers.push(oThumbViewer);

				var width = 0;
				if (i != 1) width = this.thumbContainer.width();
				this.thumbContainer.width(width + eThumbControl.width() + 15);

				if (i == 1) {
					this.setSelectedPage(0);
					this.aThumbViewers[this.iSelectedPage].setSelected(true);
					this.aPageViewers[this.iSelectedPage].load();
				}
			}

			this.bInitialized = numberOfPages > 0;

			if (this.label != null) {
				var template = this.labelEditable ? DocumentViewerTemplates.LABEL_EDITABLE : DocumentViewerTemplates.LABEL;
				var eLabel = pagesContainer.insertAfter(template.replace(TEMPLATE_SEPARATOR + CLEAR + TEMPLATE_SEPARATOR, DocumentViewerLang[this.sLang].Clear).replace(TEMPLATE_SEPARATOR + ACCEPT + TEMPLATE_SEPARATOR, DocumentViewerLang[this.sLang].Accept), true);
				var ePagesLabel = pagesContainer.get(0).querySelector(".pagesLabel").first();
				var eDialog = eLabel.select(".dialog").first();
				eDialog.width(metadata.pages[1].width * this.fZoomFactor);
				var eTitle = eLabel.select(".title").first();

				if (this.labelEditable) {
					eTitle.value = this.label;
					eTitle.on("keyup", this.atLabelKeyUp, this);
					eTitle.on("focus", this.atLabelFocus, this);
					eTitle.on("blur", this.atLabelBlur, this);
					var eClear = eDialog.select(".clear").first();
					eClear.on("click", this.atClear, this);
					var eAccept = eDialog.select(".accept").first();
					eAccept.on("click", this.renameLabel, this);
				}
				else {
					//eTitleinnerHTML = this.label;
					eTitle.style.display = "none";
				}

				if (this.labelVisible)
					ePagesLabel.style.display = "block";
				else
					ePagesLabel.style.display = "none";
			}

		} else {
			DocumentViewer.appendElement(pagesContainer, DocumentViewerLang[this.sLang].DocumentInProgress
					.replace(TEMPLATE_SEPARATOR + TIME + TEMPLATE_SEPARATOR,
						metadata.estimatedTimeToFinish));
			setTimeout(this.load.bind(this), metadata.estimatedTimeToFinish / 2 * 1000);
		}

	},

	renameLabel: function (event) {
		var pagesContainer = $(this.pagesContainer).get(0);
		var eTitle = pagesContainer.querySelector(".dialog .title").get(0);
		if (eTitle.value == "") {
			window.clearTimeout(this.blurTimeout);
			return;
		}
		if (this.onDocumentLabelChange) this.onDocumentLabelChange(eTitle.value);
	},

	atLabelKeyUp: function (event) {
		var codeKey = event.getKey();
		if (codeKey == event.ENTER) this.renameLabel();

		var pagesContainer = $(this.pagesContainer);
		var eTitle = pagesContainer.querySelector(".dialog .title").get(0);
		var eClear = pagesContainer.querySelector(".dialog .clear").get(0);
		eClear.style.display = (eTitle.value != "") ? "block" : "none";
	},

	atLabelFocus: function (event) {
		var pagesContainer = $(this.pagesContainer);
		var eTitle = pagesContainer.querySelector(".dialog .title").get(0);
		var eAccept = pagesContainer.querySelector(".dialog .accept").get(0);
		eTitle.addClass("active");
		eAccept.style.display = "block";
	},

	atPageClick: function (pageViewer) {
		if (this.onDocumentClick) this.onDocumentClick();
	},

	blurLabel: function () {
		var pagesContainer = $(this.pagesContainer);
		var eTitle = pagesContainer.querySelector(".dialog .title").get(0);
		var eAccept = pagesContainer.querySelector(".dialog .accept").get(0);
		eTitle.removeClass("active");
		eAccept.style.display = "none";
	},

	atLabelBlur: function (event) {
		this.blurTimeout = window.setTimeout(this.blurLabel.bind(this), 100);
	},

	atClear: function () {
		var pagesContainer = $(this.pagesContainer);
		var eTitle = pagesContainer.querySelector(".dialog .title").get(0);
		var eClear = pagesContainer.querySelector(".dialog .clear").get(0);
		eTitle.value = "";
		eClear.style.display = "none";
	},

	onScrollPageView: function () {
		if (this.scrolling)
			window.clearTimeout(this.scrolling);

		this.scrolling = window.setTimeout(this.doLoadPage.bind(this), 20);
	},

	doLoadPage: function() {
		var viewer = this.aPageViewers[this.iSelectedPage];
		var currentPos = $(this.pageViewPort).scrollTop();
		var page = Math.round((currentPos-100)/viewer.getPageHeight());
		this.changePage(page);
	},

	checkPageLoad: function () {
		var viewport = $(this.pageViewPort);
		var viewer;
		if (this.iSelectedPage > 0) {
			viewer = this.aPageViewers[this.iSelectedPage - 1];
			if (viewer.getPageBottom() > viewport.offset().top)
				viewer.load();
		}

		if (this.iSelectedPage < this.aPageViewers.length - 1) {
			viewer = this.aPageViewers[this.iSelectedPage + 1];
			if (viewer.getPageTop() < viewport.height() + viewport.offset().top)
				viewer.load();
		}
	},

	changePage: function (newPage, bSelectPageView) {
		if (newPage >= 0 && newPage < (this.aThumbViewers.length) && this.iSelectedPage != newPage) {
			this.aThumbViewers[this.iSelectedPage].setSelected(false);
			this.setSelectedPage(newPage);
			var viewer = this.aPageViewers[this.iSelectedPage];
			viewer.load();
			if (bSelectPageView)
				viewer.select(this.pageViewPort);
			this.aThumbViewers[this.iSelectedPage].setSelected(true);
		}
	},

	setSelectedPage: function (iNewValue) {
		this.iSelectedPage = iNewValue;
		this.currentPage.value = this.iSelectedPage + 1;
	},

	onThumbSelected: function (thumbViewer) {
		var iNewPage = thumbViewer.getId() - 1;
		if (this.iSelectedPage != iNewPage) {
			thumbViewer.setSelected(true);
			this.aThumbViewers[this.iSelectedPage].setSelected(false);
			this.setSelectedPage(iNewPage);
		}

		this.aPageViewers[this.iSelectedPage].select(this.pageViewPort, false);
	},

	onThumbOutOfViewport: function (thumbViewer) {
		if (this.scrollingThumb)
			window.clearTimeout(this.scrollingThumb);

		this.scrollingThumb = window.setTimeout(this.doScrollThumb.bind(this, thumbViewer), 100);
	},

	doScrollThumb: function (thumbViewer) {
		var scrollTo = thumbViewer;
		var containerEl = $(this.thumbsContainer);
		containerEl.animate({scrollLeft: containerEl.offset().left + scrollTo.offset().left});
	}

};