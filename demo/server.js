var open = require('open');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require('fs');

app.use(express.static('../../'));
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/document/:id', function (req, res, next) {
	var documentId = req.params["id"];
	res.header("Content-Type", "application/json");
	res.send(JSON.stringify(metadata(documentId)));
});

app.get('/document/:id/page/:page', function (req, res, next) {
	var page = req.params["page"];
	var img = fs.readFileSync('./document/pages/page-' + page + '.jpg');
    res.writeHead(200, {'Content-Type': 'image/gif' });
    res.end(img, 'binary');
});

app.get('/document/:id/page/:page/thumb', function (req, res, next) {
	var page = req.params["page"];
	var img = fs.readFileSync('./document/thumbs/page-' + page + '.jpg');
    res.writeHead(200, {'Content-Type': 'image/gif' });
    res.end(img, 'binary');
});

var server = app.listen(8888, function () {
    var host = server.address().address;
    var port = server.address().port;

    open('http://localhost:8888/cotton-document-viewer/demo/');

    console.log('Example app listening at http://%s:%s', host, port);
});

function metadata(documentId) {
	var result = {};
	result.deprecated = false;
	result.hasPendingOperations = false;
	result.numberOfPages = 20;
	result.documentId = documentId;
	result.estimatedTimeToFinish = 0;
	result.pages = {
		1: { id: 1, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		2: { id: 2, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		3: { id: 3, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		4: { id: 4, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		5: { id: 5, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		6: { id: 6, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		7: { id: 7, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		8: { id: 8, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		9: { id: 9, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		10: { id: 10, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		11: { id: 11, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		12: { id: 12, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		13: { id: 13, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		14: { id: 14, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		15: { id: 15, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		16: { id: 16, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		17: { id: 17, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		18: { id: 18, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		19: { id: 19, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 },
		20: { id: 20, height: 1650, width: 1275, aspectRatio: 0.7069306969642639 }
	};
	return result;
}
